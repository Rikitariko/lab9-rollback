import java.sql.*;
import java.util.LinkedList;

public class Main {
    // JDBC driver name and database URL
    static final String DB_URL = "jdbc:postgresql://localhost:5432/emp";

    //  Database credentials
    static final String USER = "postgres";
    static final String PASS = "admin";

    public static void main(String[] args) {
        LinkedList<Integer> ids = new LinkedList<>();
        LinkedList<Integer> employee_ids = new LinkedList<>();
        LinkedList<Integer> amount = new LinkedList<>();

        for (int i = 0; i < 5; i++) {
            ids.add(i + 1);
            employee_ids.add(i + 1 + 10 * i);
            amount.add(i + 100 * i);
        }

        add_salaries(ids, employee_ids, amount);

//        Connection conn = null;
//        Statement stmt = null;
//        try{
//            Class.forName("org.postgresql.Driver");
//            System.out.println("Connecting to database...");
//            conn = DriverManager.getConnection(DB_URL,USER,PASS);
//            conn.setAutoCommit(false);
//
//            System.out.println("Creating statement...");
//            stmt = conn.createStatement(
//                    ResultSet.TYPE_SCROLL_INSENSITIVE,
//                    ResultSet.CONCUR_UPDATABLE);
//
//            System.out.println("Inserting one row....");
//            String SQL = "INSERT INTO Employees " +
//                    "VALUES (108, 20, 'Jane', 'Eyre')";
//
//            stmt.executeUpdate(SQL);
//            // Commit data here.
//            System.out.println("Commiting data here....");
//            conn.commit();
//
//            System.out.println("Inserting one row....");
//            SQL = "INSERT INTO Employees " +
//                    "VALUES (109, 20, 'David', 'Rochester')";
//
//            stmt.executeUpdate(SQL);
//
//            System.out.println("Inserting one row....");
//            SQL = "INSERT INTO Employees " +
//                    "VALUES (108, 20, 'Rita', 'Tez')";
//
//            stmt.executeUpdate(SQL);
//            SQL = "INSERT INTO Employees " +
//                    "VALUES (107, 22, 'Sita', 'Singh')";
//            stmt.executeUpdate(SQL);
//
//            // Commit data here.
//            System.out.println("Commiting data here....");
//            conn.commit();
//
//            String sql = "SELECT id, first, last, age FROM Employees";
//            ResultSet rs = stmt.executeQuery(sql);
//            System.out.println("List result set for reference....");
//            printRs(rs);
//            rs.close();
//            stmt.close();
//            conn.close();
//        }catch(SQLException se){
//            //Handle errors for JDBC
//            se.printStackTrace();
//            // If there is an error then rollback the changes.
//            System.out.println("Rolling back data here....");
//            try{
//                if(conn!=null)
//                    conn.rollback();
//            }catch(SQLException se2){
//                se2.printStackTrace();
//            }//end try
//
//        }catch(Exception e){
//            //Handle errors for Class.forName
//            e.printStackTrace();
//        }finally{
//            //finally block used to close resources
//            try{
//                if(stmt!=null)
//                    stmt.close();
//            }catch(SQLException se2){
//            }
//            try{
//                if(conn!=null)
//                    conn.close();
//            }catch(SQLException se){
//                se.printStackTrace();
//            }//end finally try
//        }
//        System.out.println("Goodbye!");
//    }
}
    public static void add_salaries(LinkedList<Integer> ids, LinkedList<Integer> employee_ids, LinkedList<Integer> amount) {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            conn.setAutoCommit(false);

            System.out.println("Creating statement...");
            stmt = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            String SQL;
            for (int i = 0; i < ids.size(); i++) {
                SQL = "INSERT INTO Salary " +
                        "VALUES (" + ids.get(i) + "," +
                        employee_ids.get(i) + "," +
                        amount.get(i) + ")";
                stmt.executeUpdate(SQL);
            }
            conn.commit();
            String sql = "SELECT id, employee_id, amount FROM Salary";
            ResultSet rs = stmt.executeQuery(sql);
            printRs(rs);
            rs.close();
            stmt.close();
            conn.close();
        } catch(ClassNotFoundException se) {
            //Handle errors for JDBC
            se.printStackTrace();
            // If there is an error then rollback the changes.
            System.out.println("Rolling back data here....");
        } catch(Exception e){
            e.printStackTrace();
        } finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }
        System.out.println("Goodbye!");
    }

    public static void printRs(ResultSet rs) throws SQLException{
        //Ensure we start with first row
        rs.beforeFirst();
        while(rs.next()) {
            //Retrieve by column name
            int id  = rs.getInt("id");
            int employee_id = rs.getInt("employee_id");
            int amount = rs.getInt("amount");

            //Display values
            System.out.println("ID: " + id);
            System.out.println(", Employee_id: " + employee_id);
            System.out.println(", Amount: " + amount);
        }
        System.out.println();
    }
}
